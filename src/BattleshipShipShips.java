import java.util.ArrayList;
import java.util.List;

/* Group of BattleshipShips - One Board */

public class BattleshipShipShips {
	private List<BattleshipShip> ships = new ArrayList<BattleshipShip>();
	public BattleshipShipShips() {}
	public void add (BattleshipShip ship) {
		this.ships.add(ship);
	}
	
	public boolean addPegTo(int row, int col) {
		for(BattleshipShip ship : this.ships) {
			if(ship.addPegTo(row, col))
				return true;
		}
		return false;
	}
	
	public List<BattleshipShip> getList() {
		return this.ships;
	}
}

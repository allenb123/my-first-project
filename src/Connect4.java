import java.util.Scanner;
import java.util.InputMismatchException;

public class Connect4 {

	static void print_board() {
		System.out.printf(board_string + "\n", (Object[])board_vals);
	}
	
	static int get_index_for(int row, int col) {
		return (row - 1) * 7 + col - 1;
	}
	
	// Who won?
	static char who_won = 0;
	static boolean somebody_wins() {
		// For each lane - vertical
		for(int i = 1; i <= 7; i++) {
			// For each row from 1-3
			for(int j = 1; j <= 3; j++) {
				if(board_vals[get_index_for(j, i)] == board_vals[get_index_for(j + 1, i)] &&
						board_vals[get_index_for(j + 1, i)] == board_vals[get_index_for(j + 2, i)] &&
						board_vals[get_index_for(j + 2, i)] == board_vals[get_index_for(j + 3, i)] &&
						board_vals[get_index_for(j, i)] != ' ') {
					who_won = board_vals[get_index_for(j, i)];
					return true;
				}
			}
		}
		
		// For each row - horizontal
		for(int i = 1; i <= 6; i++) {
			// For each lane from 1-4
			for(int j = 1; j <= 4; j++) {
				if(board_vals[get_index_for(i, j)] == board_vals[get_index_for(i, j + 1)] &&
						board_vals[get_index_for(i, j + 1)] == board_vals[get_index_for(i, j + 2)] &&
						board_vals[get_index_for(i, j + 2)] == board_vals[get_index_for(i, j + 3)] &&
						board_vals[get_index_for(i, j)] != ' ') {
					who_won = board_vals[get_index_for(i, j)];
					return true;
				}
			}
		}
		
		// Diagonal from ttb (top to bottom) (ltr)
		for(int i = 1; i <= 3; i++) {
			// j = column #
			for(int j = 1; j <= 4; j++) {
				if(board_vals[get_index_for(i, j)] == board_vals[get_index_for(i + 1, j + 1)] && 
						board_vals[get_index_for(i + 1, j + 1)] == board_vals[get_index_for(i + 2, j + 2)] && 
						board_vals[get_index_for(i + 2, j + 2)] == board_vals[get_index_for(i + 3, j + 3)] &&
						board_vals[get_index_for(i, j)] != ' ') {
					who_won = board_vals[get_index_for(i, j)];
					return true;
				}
			}
		}
		
		// Diagonal btm to top
		for(int i = 1; i <= 3; i++) {
			// J = column
			for(int j = 1; j <= 7; j++) {
				if(board_vals[get_index_for(i, j)] == board_vals[get_index_for(i + 1, j - 1)] && 
						board_vals[get_index_for(i + 1, j - 1)] == board_vals[get_index_for(i + 2, j - 2)] && 
						board_vals[get_index_for(i + 2, j - 2)] == board_vals[get_index_for(i + 3, j - 3)] &&
						board_vals[get_index_for(i, j)] != ' ') {
					who_won = board_vals[get_index_for(i, j)];
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println("Connect 4\n" +
							"---------");
		
		// Who's turn it is - 'R' is red, 'B' is blue
		char turn = 'R';
		
		// Loop
		for(;;) {
			print_board();
			
			System.out.print("Player " + turn + ", Enter a number: ");
			int n = 0;
			try {
				n = scan.nextInt();
			} catch(InputMismatchException e) {
				System.err.println("That's not a number");
				scan.nextLine();
				continue;
			}
			if(n > 7 || n < 1) {
				System.err.println("Enter a number from 1-7, please");
				continue;
			}
			
			// Find where to put the piece
			for(int i = 6; true; i--) {
				if(i <= 0) {
					System.err.println("Lane filled!");
					break;
				}
				if(board_vals[get_index_for(i, n)] != 'R' && board_vals[get_index_for(i, n)] != 'B') {
					// Put it there
					board_vals[get_index_for(i, n)] = turn;
					break;
				}
			}
			
			if(somebody_wins())
				break;

			// Set turn to the opposite player
			if(turn == 'R')
				turn = 'B';
			else
				turn = 'R';
		}
		
		print_board();
		
		System.out.println(who_won + " won!");
	}

	final static Scanner scan = new Scanner(System.in);
	final static String board_string = 
			"\n| 1 | 2 | 3 | 4 | 5 | 6 | 7 |" +
			"\n+---+---+---+---+---+---+---+\n" +
			String.format("%0" + 6 + "d", 0).replace("0", "| %c | %c | %c | %c | %c | %c | %c |\n");
	static Character[] board_vals = {
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ',
			' ', ' ', ' ', ' ', ' ', ' ', ' ', 0};
}

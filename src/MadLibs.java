import java.util.Scanner;

public class MadLibs {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		String[] stuff = {"noun", "plural noun", "verb with -ing"};
		String[] more_stuff = new String[3];
		for(int i = 0; i < stuff.length; i++) {
			System.out.print("Enter a " + stuff[i] + ": ");
			more_stuff[i] = scan.nextLine();
		}
		System.out.println("\nHi, I'm a " + more_stuff[0] + " and I like " + more_stuff[1] + ". I also like " + more_stuff[2] + ".");
	}
}
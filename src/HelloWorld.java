import java.util.Scanner;

public class HelloWorld {
	public static void main(String[] args) {
		System.out.print("Some other thing out using just print\n");
		System.out.print("Something else now\n");
		
		int integer_variable = 0;
		System.out.println(integer_variable);
		Scanner scan = new Scanner(System.in);
		String something = scan.nextLine();
		System.out.println(something);
	}
}

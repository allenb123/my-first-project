import java.util.Arrays;
import java.util.Random;

import BinarySearch;

public class InsertSort {	
	private static Random rand = new Random();
	
	public static int[] insertSort(int[] arr) {
		for(int i = 1; i < arr.length; i++) {
			int k;
			for(int j = k = i; j >= 0; j--) {
				System.out.print(arr[k] + " ");
				if(arr[k] >= arr[j]) {
					System.out.print(">=");
				}
				else {
					System.out.print("<");
					// swap arr[i] and ar r[j]
					int temp = arr[k];
					arr[k] = arr[j];
					arr[j] = temp;
					k = j;
				}
				System.out.println(" " + arr[j]);
			}
			System.out.println(Arrays.toString(arr) + "\n");
		}
		return arr;
	}
	
	static int[] generateRandomArray() {
		int[] arr = new int[rand.nextInt(10) + 3];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = (int)rand.nextInt(100); 
		}
		return arr;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = generateRandomArray();
		System.out.println(Arrays.toString(arr) + "\n");
		System.out.println(Arrays.toString(insertSort(arr)));
	}
}

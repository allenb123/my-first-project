
public class Card {
	private int number;
	private String suit;
	
	public Card(int number, String suit) {
		this.number = number;
		this.suit = suit;
	}
	
	public String toString() {
		return this.number + " of " + this.suit;
	}
	
	public int getNumber() {
		return this.number;
	}
}

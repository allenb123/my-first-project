
public class War {
	static Deck main_deck = Deck.shuffled();
	static Deck player_one_deck = null;
	static Deck player_two_deck = null;
	static Deck on_hold = new Deck(0);
	
	public static void main(String[] args) {
		player_one_deck = main_deck.subDeck(0, main_deck.size() / 2);
		player_two_deck = main_deck.subDeck(main_deck.size() / 2, -1);

		for(;;) {
			Card c = player_one_deck.draw();
			Card d = player_two_deck.draw();
			System.out.println(c + " from P1 and " + d + " from P2");
			on_hold.addAll(new Card[] {c, d});
			if(c.getNumber() > d.getNumber()) {
				player_one_deck.addAll(on_hold.getList());
				on_hold.clear();
				System.out.println("Player 1 wins the round");
			} else if(c.getNumber() < d.getNumber()) {
				player_two_deck.addAll(on_hold.getList());
				on_hold.clear();
				System.out.println("Player 2 wins the round");
			} else {
				System.out.println("Tie!");
			}
			
			if(c.getNumber() != d.getNumber()) {
				System.out.println("P1 now has " + player_one_deck.size() + " cards");
				System.out.println("P2 now has " + player_two_deck.size() + " cards");
			}
				
			if(player_one_deck.size() == 0) {
				System.out.println("Player two wins!");
				break;
			} if(player_two_deck.size() == 0) {
				System.out.println("Player one wins!");
				break;
			}
		}
		
	}
}

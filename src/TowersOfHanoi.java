import java.util.Scanner;
import java.util.Arrays;

/*
 3
***
A -> C /small
A -> B /med
C -> B /small
A -> C /large
B -> A /small
B -> C /med
A -> C /small

 4
***
A -> B /small
A -> C /med
B -> C /small
A -> B /large
C -> A /small
C -> B /med
A -> B /small
A -> C /huge
B -> C /small
B -> A /med
C -> A /small
B -> C /large
A -> B /small
A -> C /med
B -> C /small
*/

public class TowersOfHanoi {
	static Scanner scan = new Scanner(System.in);
	
	static void solve (char src, char dest, int discs, final int orig_discs) {
		char newsrc = 0;
		
		if(src != 'A' && dest != 'A') newsrc = 'A';
		if(src != 'B' && dest != 'B') newsrc = 'B';
		if(src != 'C' && dest != 'C') newsrc = 'C';
		
		if(discs == -1)
			return;
						
		solve(src, newsrc, discs - 1, orig_discs);
		
		if(discs != orig_discs) {
			System.out.println(src + " => " + dest + ": " + discs);
			solve(newsrc, dest, discs - 1, orig_discs);
		}
	}
	
	static void solve_wrap(char src, char dest, int discs) {
		solve(src, dest, discs, discs);
	}
	
	public static void main(String[] args) {
		System.out.print("Enter discs: ");
	 	
		solve_wrap('A', 'B', scan.nextInt());
		return;
	}
}
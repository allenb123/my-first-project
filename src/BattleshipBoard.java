import java.util.ArrayList;

public class BattleshipBoard {
	private static String board = 
			String.format("%0" + 10 + "d", 0).replace("0","| %c | %c | %c | %c | %c | %c | %c | %c | %c | %c |\n");
	private BattleshipShipShips ships1; // ships for player 1
	private BattleshipShipShips ships2; // ships for player 2

	public BattleshipBoard(BattleshipShipShips ships1, BattleshipShipShips ships2) {
		this.ships1 = ships1;
		this.ships2 = ships2;
	}
	public void print(BattleshipPeg[] pegs, int player) {
		ArrayList<Character> pegsb = new ArrayList<Character>();
		for(BattleshipPeg peg: pegs) {
			pegsb.add(peg == BattleshipPeg.None ? ' ' : (
					peg == BattleshipPeg.Red ? 'R' : 'W'
			));
		}
		if(player != 0)
			for(BattleshipShip ship: (player == 1 ? ships1 : ships2).getList()) {
				if(ship.dir) { // top to bottom
					for(int r = ship.row; r < ship.size() + ship.row; r++)
						pegsb.set(r * 10 + ship.col, 'O');
				} else { // ltr
					for(int r = ship.col; r < ship.size() + ship.col; r++)
						pegsb.set(ship.row * 10 + r, 'O');
				}
			}
		System.out.printf(board, (Object[])pegsb.toArray());
	}
}

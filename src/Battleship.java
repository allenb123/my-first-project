import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Battleship {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		BattleshipShipShips ships1 = new BattleshipShipShips(); // player 1
		BattleshipShipShips ships2 = new BattleshipShipShips(); // player 2
		
		BattleshipBoard board = new BattleshipBoard(ships1, ships2);
		BattleshipPeg[] peg_data1 = new BattleshipPeg[101];
		BattleshipPeg[] peg_data2 = new BattleshipPeg[101];
		Arrays.fill(peg_data1, BattleshipPeg.None);
		Arrays.fill(peg_data2, BattleshipPeg.None);
		
		System.out.println("Player 2, go away");
		scan.nextLine();
		
		System.out.println("Player 1, respond to the following questions.");
		
		for(int i = 5; i >= 2; i--) {
			System.out.print("For ship " + i + ", enter the row:    ");
			int row = scan.nextInt();
			System.out.print("For ship " + i + ", enter the column: ");
			int col = scan.nextInt();
			System.out.print("For ship " + i + ", which direction? (1 is top to bottom, 0 is left to right): ");
			
			ships1.add(new BattleshipShip(i, row, col, scan.nextInt() != 0));
		}

		System.out.println("\nThis is your board:");
		board.print(peg_data1, 1);
		scan.nextLine();
		scan.nextLine();

		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out.println("Player 2, come back");
		System.out.println("Player 1, go away\n");
		System.out.println("Player 2, respond to the following questions.");
		
		for(int i = 5; i >= 2; i--) {
			System.out.print("For ship " + i + ", enter the row:    ");
			int row = scan.nextInt();
			System.out.print("For ship " + i + ", enter the column: ");
			int col = scan.nextInt();
			System.out.print("For ship " + i + ", which direction? (1 is top to bottom, 0 is left to right): ");
			
			ships2.add(new BattleshipShip(i, row, col, scan.nextInt() != 0));
		}
		
		System.out.println("\nThis is your board:");
		board.print(peg_data2, 2);
		scan.nextLine();
		scan.nextLine();
		
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		
		for(;;) {
			System.out.print("Player 1: Enter row: ");
			int row = scan.nextInt() - 1;
			System.out.print("Player 1: Enter column: ");
			int col = scan.nextInt() - 1;
			
			if(ships1.addPegTo(row, col)) {
				BattleshipPeg.setPegAt(peg_data2, row, col, BattleshipPeg.Red);
			} else {
				BattleshipPeg.setPegAt(peg_data2, row, col, BattleshipPeg.White);
			}
			board.print(peg_data2, 0);
			
			System.out.print("Player 2: Enter row: ");
			row = scan.nextInt() - 1;
			System.out.print("Player 2: Enter column: ");
			col = scan.nextInt() - 1;
			
			if(ships1.addPegTo(row, col)) {
				BattleshipPeg.setPegAt(peg_data1, row, col, BattleshipPeg.Red);
			} else {
				BattleshipPeg.setPegAt(peg_data1, row, col, BattleshipPeg.White);
			}
			board.print(peg_data1, 0);
		}
		
	}
}

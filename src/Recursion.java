import java.util.Scanner;

public class Recursion {
	
	// Print out all powers and then return base^pow
	static long getPowerOf(long base, long pow) {
		if(pow == 0)
			return 1;
		long i = getPowerOf(base, pow - 1) * base;
		System.out.println(i);
		return i;
	}
	
	// 1, 1, 2, 3, 5, 8, etc.
	static long getFib(long n, boolean print) {
		if(n == 0 || n == 1) {
			if(print)
				System.out.println(1);
			return 1;
		}
		long i = getFib(n - 1, true && print) + getFib(n - 2, false);
		if(print)
			System.out.println(n + ": " + i);
		return i;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		/* System.out.print("Enter the base: ");
		long base = scan.nextLong();
		System.out.print("Enter the exponent: ");	
		long pow = scan.nextLong();
		getPowerOf(base, pow);*/
		System.out.print("How many fib. numbers do you want?: ");
		getFib(scan.nextInt(), true);
	}
}

import java.util.Arrays;
import java.util.Random;

public class BubbleSort {
	private static Random rand = new Random();
	
	public static double[] bsort(double[] arr) {
		for(int i = arr.length - 1; i > 0; i--) {
			for(int j = 0; j < i; j++) {
				if(arr[j] > arr[j + 1]) {
					double temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		return arr;
	}
	
	static double[] generateRandomArray() {
		double[] arr = new double[rand.nextInt(10) + 3];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = (double)rand.nextInt(100) + 0.1 * rand.nextInt(10); 
		}
		return arr;
	}
	
	public static void main(String[] args) {
		double[] arr = generateRandomArray();
		
		System.out.println(Arrays.toString(arr));
		
		// TODO Auto-generated method stub
		System.out.println(
			Arrays.toString(
				bsort(arr)
			)
		);
	}

}

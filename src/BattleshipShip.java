
public class BattleshipShip {
	private final int size;
	private BattleshipPeg[] data;
	public final int row;
	public final int col;
	public final boolean dir; // true is top to bottom, false is left to right
	
	public BattleshipShip(int size) {
		this.row = 0;
		this.col = 0;
		this.size = size;
		this.data = new BattleshipPeg[size];
		this.dir = true;
	}

	public BattleshipShip(int size, int row, int col, boolean dir) {
		this.row = row;
		this.col = col;
		this.size = size;
		this.data = new BattleshipPeg[size];
		this.dir = dir;
	}
	
	public int size() {
		return this.size;
	}

	public void addPeg(int spot, BattleshipPeg peg) {
		if(this.size <= spot) {
			return;
		}
		this.data[spot] = peg;
	}
	
	public boolean isSunk() {
		boolean isSunk = true;
		for (BattleshipPeg peg: this.data) {
			if(peg == BattleshipPeg.None) {
				isSunk = false;
			}
		}
		return isSunk;
	}
	
	public boolean addPegTo(int row, int col) {
		if(this.dir) { // top to bottom
			for(int r = this.row; r < this.size() + this.row; r++)
				if(row == r && col == this.col) {
					this.data[r - this.row] = BattleshipPeg.Red;
					return true;
				}
		} else { // ltr
			for(int r = this.col; r < this.size() + this.col; r++)
				if(row == this.row && col == r) {
					this.data[r - this.col] = BattleshipPeg.Red;
					return true;
				}
		}
		return false;
	}
}
public enum BattleshipPeg {
	None,
	White,
	Red;
	
	public static BattleshipPeg getPegAt(BattleshipPeg[] board, int row, int col) {
		return board[row * 10 + col];
	}
	
	public static BattleshipPeg setPegAt(BattleshipPeg[] board, int row, int col, BattleshipPeg peg) {
		BattleshipPeg old = board[row * 10 + col];
		board[row * 10 + col] = peg;
		return old;
	}
}

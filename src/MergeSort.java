import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MergeSort {
	static Random rand = new Random();
	static Scanner scan = new Scanner(System.in);
	
	static int[] msort(int[] arr) {
		if(arr.length <= 2) {
			if(arr.length >= 2 && arr[0] > arr[1]) {
				int temp = arr[0];
				arr[0] = arr[1];
				arr[1] = temp;
			}
			System.out.println(Arrays.toString(arr));
			return arr;
		}
		int[] arr1 = msort(Arrays.copyOfRange(arr, 0, arr.length / 2));
		int[] arr2 = msort(Arrays.copyOfRange(arr, arr.length / 2, arr.length));
		
		arr = new int[arr.length];
		int arr1ctr = 0;
		int arr2ctr = 0;
		for(int i = 0; i < arr.length; i++) {
			if(arr1ctr < arr1.length && arr2ctr < arr2.length && arr1[arr1ctr] <= arr2[arr2ctr]) {
				arr[i] = arr1[arr1ctr];
				arr1ctr++;
			} else if(arr2ctr < arr2.length) {
				arr[i] = arr2[arr2ctr];
				arr2ctr++;
			} else if(arr1ctr < arr.length) {
				arr[i] = arr1[arr1ctr];
				arr1ctr++;
			}
		}
		
//		arr = Arrays.copyOf(arr1, arr1.length + arr2.length);
//		System.arraycopy(arr2, 0, arr, arr1.length, arr2.length);
		
		System.out.println(Arrays.toString(arr));
		return arr;
	}
	
	static int[] generateRandomArray(int n) {
		int[] arr = new int[rand.nextInt(10) + 3];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = (int)rand.nextInt(n + 1); 
		}
		return arr;
	}
	
	public static void main(String[] args) {
		int[] list = generateRandomArray(scan.nextInt());
		System.out.println(Arrays.toString(list));
		msort(list);
		return;
	}
}

import java.util.Collections;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Deck {
	private List<Card> cards = new ArrayList<Card>();
	
	public Deck() {
		String[] suits = {"Hearts", "Diamonds", "Spades", "Clubs"};
		for(String suit: suits) {
			for(int i = 1; i <= 13; i++) {
				cards.add(new Card(i, suit));
			}
		}
	}
	
	public Deck(List<Card> cards) {
		this.cards = cards;
	}
	
	public Deck(int n) {
		this.cards = new ArrayList<Card>(n);
	}
	
	public List<Card> getList() {
		return this.cards;
	}
	
	public int size() {
		return this.cards.size();
	}
	
	public static Deck shuffled() {
		Deck deck = new Deck();
		deck.shuffle();
		return deck;
	}
	
	public void shuffle() {
		Collections.shuffle(cards);
	}
	
	public Card add(Card card) {
		this.cards.add(0, card);
		return card;
	}
	
	public void addAll(Card[] cards) {
		for(Card card: cards) {
			this.cards.add(card);
		}
	}

	public void addAll(Collection<Card> cards) {
		this.cards.addAll(cards);
	}
	
	public Card draw() {
		Card card = cards.get(cards.size() - 1);
		cards.remove(cards.size() - 1);
		return card;
	}
	
	public void clear() {
		cards.clear();
	}
	
	public String toString() {
		return this.cards.toString();
	}
	
	public Deck subDeck(int from, int to) {
		if(to == -1)
			to = this.size();
		return new Deck(new ArrayList<Card>(this.getList()).subList(from, to));
	}
}

import java.util.Random;
import java.util.Scanner;
import java.util.InputMismatchException;

public class AGuessingGame {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("A Guessing Game");
		System.out.println("---------------\n");
		
		Random rand = new Random();
		Scanner scan = new Scanner(System.in);
		long tries = 0;

		int real = rand.nextInt(20) + 1;
		
		for(;;) {
			tries++;
			System.out.print("Enter a number beetween 1 and 20: ");
			int guess = 0;
			try {
				guess = scan.nextInt();
			} catch(InputMismatchException e) {
				System.out.println("You didn't enter a number");
				scan.nextLine();
				continue;
			}
			if(real == guess) { 
				System.out.println("Congratulations! You were right! The answer was " + real + "!");
				System.out.println("You only took " + tries + " tries to get it right!");
				break;
			} else {
				System.out.println("Oops! You were wrong!");
			}
		}
	}
}

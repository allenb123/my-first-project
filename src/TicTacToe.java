import java.util.Scanner;

public class TicTacToe {	
	static void init() {
		System.out.println("Tic Tac Toe");
		System.out.println("===========\n");
	}
	
	static void print_board() {
		System.out.printf(board_string, all[0], all[1], all[2], all[3], all[4], all[5], all[6], all[7], all[8]);
	}
	
	static boolean is_filled() {
		boolean is_fill = true;
		for(char item: all) {
			if(item != 'X' && item != 'O')
				is_fill = false;
		}
		return is_fill;
	}
	
	public static void main(String[] args) {		
		init();
		char turn = 'X';
		char win = 0;
		for(;;) {
			print_board();
			System.out.print("Player " + turn + ", Enter a number: ");
			int n = scan.nextInt();
			if(Character.isDigit(all[n - 1]))
				all[n - 1] = turn;
			else {
				System.err.println("That space is already taken");
				continue;
			}
			
			if(is_filled())
				break;
			
			
			if((all[0] == all[1] && all[1] == all[2]) || (all[0] == all[4] && all[4] == all[8]) || (all[0] == all[3] && all[3] == all[6])) {
				win = all[0];
			} else if(all[1] == all[4] && all[4] == all[7]) {
				win = all[1];
			} else if((all[2] == all[5] && all[5] == all[8]) || (all[2] == all[4] && all[4] == all[6])) {
				win = all[2];
			} else if(all[3] == all[4] && all[4] == all[5]) {
				win = all[3];
			} else if(all[6] == all[7] && all[7] == all[8]) {
				win = all[6];
			}
			
			if(win != 0)
				break;
			
			switch(turn) {
			case 'X':
				turn = 'O';
				break;
			case 'O':
				turn = 'X';
			}
		}
		
		print_board();
				
		System.out.println("---------");
		System.out.println("Game over");
		if(win != 0)
			System.out.println(win + " wins!");
		else
			System.out.println("Tie!");
	}

	private final static Scanner scan = new Scanner(System.in);
	private static char[] all = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
	private final static String board_string = "\n%c | %c | %c\n%c | %c | %c\n%c | %c | %c\n\n";
}

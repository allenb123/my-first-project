

public class BinarySearch {
	public static int bsearch(int[] array, int searchfor) {
		return bsearch(array, searchfor, 0, array.length - 1);
	}
	private static int bsearch(int[] array, int searchfor, int min, int max) {
		System.out.println((min + max) / 2);
		if(array[(min + max) / 2] == searchfor)
			return (min + max) / 2;
		else if(array[max] == searchfor)
			return max;
		//not in array
		else if(min + 1 == max || min == max)
			return -1;
		else if(array[(min + max) / 2] > searchfor)
			return bsearch(array, searchfor, min, (min + max) / 2);
		else // if (array[(min + max) / 2] < searchfor)
			return bsearch(array, searchfor, (min + max) / 2, max);
	}
	public static void main (String[] args) {
		int arr[] = new int[1000];
		for(int i = 0; i < arr.length; i++)
			arr[i] = i;
		System.out.println(bsearch(arr, 999));
	}
}
